<?php

Route::group(['namespace' => 'Website'], function (){

	Route::get('/','PageController@index')->name('index');

	Route::get('contact','PageController@contact')->name('front-contact');
});