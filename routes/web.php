<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>['admin.login.authenticate'],'prefix'=>'admin-panel'],function () {

	Route::get('/', function () {
        return view('admin.auth.login');
    })->name('admin-panel.login');

    Route::any('dashboard',function(){
        return view('admin.index');
    })->name('admin-panel.dashboard');

    Route::get('logout',function(){
        return redirect()->route('admin-panel.login');
    })->name('admin-panel.logout');

});
