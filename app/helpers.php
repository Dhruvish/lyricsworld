<?php


if (!function_exists('default_response')) {
   function default_response($status=0, $message='something went wrong',$data=array())
    {
           $response['status']=$status;
           $response['message']=$message;
           if(isset($data) && !empty($data)){
               $response['data']=$data;
           }
       
       return $response;
    }
}

if (!function_exists('default_status')) {
   function default_status()
    {
       return false;
    }
}

if (!function_exists('default_message')) {
   function default_message()
    {
       return 'failed';
    }
}


if (!function_exists('jsonResponse')) {

    function jsonResponse($response, $status = 200)
    {
        $header = array(
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'utf-8'
        );
        return response()->json($response, $status);
    }
}

/*
 *  Function to create model object and call function with params given 
 *  @param : $model (String)
 *  @param : $call (Array['functionName'=>array('arguments')])      
 */
if (!function_exists('modelCall')) {

    function modelCall($model, $call)
    {
        $o_object = new $model();
        //$o_object->{$call[0]}();
        call_user_func_array(array($o_object, $call[0]), $call[1]);
    }
}

/*
 *  Function to get menu if available
 *  @param : $menu (Menu name)
 */
if (!function_exists('getSubmenu')) {

    function getMenu($menu)
    {
        $menus = \Config::get('custom.menu_details');
        if (isset($menus[$menu])) {
            return $menus[$menu];
        }
        return [];
    }
}

/*
 *  Function to get submenu if available
 *  @param : $menu (Menu name)
 */
if (!function_exists('getSubmenu')) {

    function getSubmenu($menu)
    {
        $menus = \Config::get('custom.menu_details');
        if (isset($menus[$menu]) && isset($menus[$menu]['sub-menu'])) {
            return $menus[$menu]['sub-menu'];
        }
        return [];
    }
}

/*
 *  Function to send mail
 *  @param : $mails
 *  @param : $data if its there
 */
if (!function_exists('sendMail')) {

    function sendMail($mailClass, $data = '')
    {
        dispatch(new \App\Jobs\MailNotification('\App\Mail\\' . $mailClass, $data));
    }
}

/*
 *  Function to check user is company admin
 *  @param : $user
 */
// if (!function_exists('isCompanyAdmin')) {

//     function isCompanyAdmin($user) {
//         return ($user->role_id == 3 ? true : false);
//     }

// }

/*
 *  Function to check user is company
 *  @param : $company_id
 */
if (!function_exists('isValidCompany')) {

    function isValidCompany($company_id)
    {
        $company = \App\Models\User::where('id', $company_id)->select(['id', 'name', 'email', 'phone'])->first();
        if ($company) {
            return $company;
        }   
        return false;
    }
}

/*
 *  Function to check user is belongs to requested company of not
 *  @param : $user
 */
if (!function_exists('isValidCompanyAdmin')) {

    function isValidCompanyAdmin($user)
    {
        $auth_user = \Session::get('backend-user');
        if ($user->role_id != 3) {
            return false;
        }
        if ($auth_user->role_id >= 2) {
            if ($auth_user->role_id == 2) {
                if ($auth_user->id != $user->parent_user_id) {
                    return false;
                }
            } elseif ($auth_user->role_id < 6) {
                if ($auth_user->parent_user_id != $user->parent_user_id) {
                    return false;
                }
            }
        }
        return true;
    }
}

/*
 *  Function to check user is belongs to requested company of not
 *  @param : $user
 */
if (!function_exists('isValidCompanyPropertyManager')) {

    function isValidCompanyPropertyManager($user)
    {
        $auth_user = \Session::get('backend-user');
        if ($user->role_id != 4) {
            return false;
        }
        if ($auth_user->role_id >= 2) {
            if ($auth_user->role_id == 2) {
                if ($auth_user->id != $user->parent_user_id) {
                    return false;
                }
            } elseif ($auth_user->role_id < 6) {
                if ($auth_user->parent_user_id != $user->parent_user_id) {
                    return false;
                }
            }
        }
        return true;
    }
}

/*
 *  Function to check property is belongs to requested company of not
 *  @param : $user
 */
if (!function_exists('isValidProperty')) {

    function isValidProperty($property)
    {

        $auth_user = \Session::get('backend-user');

        if ($auth_user->role_id != 1) {
            if (($auth_user->role_id == 2) && ($property->user_id == $auth_user->id)) {
                return true;
            } elseif (($auth_user->role_id > 2) && ($property->user_id == $auth_user->parent_user_id)) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}

/*
 *  Function to check job is belongs to requested company of not
 *  @param : $job
 */
if (!function_exists('isValidCompanyJob')) {

    function isValidCompanyJob($job)
    {
        $auth_user = \Session::get('backend-user');
        if ($auth_user->role_id >= 2) {
            if ($auth_user->role_id == 2) {
                if ($auth_user->id != $job->user_id) {
                    return false;
                }
            } elseif ($auth_user->role_id <= 6) {
                if ($auth_user->parent_user_id != $job->user_id) {
                    return false;
                }
            }
        }
        return true;
    }
}

/*
 *  Function to check user is belongs to requested company of not
 *  @param : $user
 */
if (!function_exists('isValidCompanyBoardMember')) {

    function isValidCompanyBoardMember($user)
    {
        $auth_user = \Session::get('backend-user');
        if ($user->role_id != 5) {
            return false;
        }
        if ($auth_user->role_id >= 2) {
            if ($auth_user->role_id == 2) {
                if ($auth_user->id != $user->parent_user_id) {
                    return false;
                }
            } elseif ($auth_user->role_id < 6) {
                if ($auth_user->parent_user_id != $user->parent_user_id) {
                    return false;
                }
            }
        }
        return true;
    }
}

/*
 *  Function to get main parent id
 */
if (!function_exists('getMainParent')) {

    function getMainParent()
    {
        $auth_user = \Session::get('backend-user');
        if ($auth_user->role_id >= 2) {
            if ($auth_user->role_id == 2) {
                return $auth_user->id;
            }
            return $auth_user->parent_user_id;
        }
        return 0;
    }
}

/*
 *  Function to get logged in user type confirmation
 *  Return Boolean
 */
if (!function_exists('isSuperAdmin')) {

    function isSuperAdmin()
    {
        $auth_user = \Session::get('backend-user');
        if ($auth_user->role_id == 1) {
            return true;
        } else {
            return false;
        }
    }
}

/*
 *  Function to get logged in user type confirmation
 *  Return Boolean
 */
if (!function_exists('isIndividual')) {

    function isIndividual()
    {
        $auth_user = \Session::get('backend-user');
        if ($auth_user->role_id == INDIVIDUAL) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('isTrainer')) {

    function isTrainer()
    {
        $auth_user = \Session::get('backend-user');
        if ($auth_user->role_id == TRAINER) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('isClub')) {

    function isClub()
    {
        $auth_user = \Session::get('backend-user');
        if ($auth_user->role_id == CLUB) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('isSubadmin')) {

    function isSubadmin()
    {
        $auth_user = \Session::get('backend-user');
        if ($auth_user->role_id == 2) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('isHumanresource')) {

    function isHumanresource()
    {
        $auth_user = \Session::get('backend-user');
        if ($auth_user->role_id == 5) {
            return true;
        } else {
            return false;
        }
    }
}

/*
 *  Function to get logged in user type confirmation
 *  Return Boolean
 */
if (!function_exists('isCompanyAdmin')) {

    function isCompanyAdmin()
    {
        $auth_user = \Session::get('backend-user');
        if ($auth_user->role_id == 3) {
            return true;
        } else {
            return false;
        }
    }
}

/*
 *  Function to get logged in user type confirmation
 *  Return Boolean
 */
if (!function_exists('isCompanyPM')) {

    function isCompanyPM()
    {
        $auth_user = \Session::get('backend-user');
        if ($auth_user->role_id == 4) {
            return true;
        } else {
            return false;
        }
    }
}

/*
 *  Function to get logged in user type confirmation
 *  Return Boolean
 */
if (!function_exists('isCompanyBM')) {

    function isCompanyBM()
    {
        $auth_user = \Session::get('backend-user');
        if ($auth_user->role_id == 5) {
            return true;
        } else {
            return false;
        }
    }
}

/*
 *  Function to get logged in user type confirmation
 *  Return Boolean
 */
if (!function_exists('isCompanyContractor')) {

    function isCompanyContractor()
    {
        $auth_user = \Session::get('backend-user');
        if ($auth_user->role_id == 6) {
            return true;
        } else {
            return false;
        }
    }
}

/*
 *  Function to get logged in user id
 *  Return Boolean
 */
if (!function_exists('getLogInUserId')) {

    function getLogInUserId()
    {
        return \Session::get('backend-user-id');
    }
}

/*
 *  Function to get logged in user id
 *  Return Boolean
 */
if (!function_exists('getLogInUserRole')) {

    function getLogInUserRole()
    {
        $loginUser = \Session::get('backend-user');
        if ($loginUser) {
            return $loginUser->role_id;
        }
        return 0;
    }
}

/*
 *  Function to get logged in user id
 *  Return Boolean
 */
if (!function_exists('getLogInUser')) {

    function getLogInUser()
    {
        $loginUser = \Session::get('backend-user');
        if ($loginUser) {
            return $loginUser;
        }
        return collect([]);
    }
}

/*
 *  Function to get logged in user name
 *  Return String
 */
if (!function_exists('getLogInUserName')) {

    function getLogInUserName()
    {
        $loginUser = \Session::get('backend-user');
        if ($loginUser) {
            return $loginUser->name;
        }
        return '';
    }
}

/*
 *  Function to get logged in user id
 *  Return Boolean
 */
if (!function_exists('getContractorCategory')) {

    function getContractorCategory()
    {
        return \App\ContractorCategory::where('user_id', getLogInUserId())->get()->pluck('category_id');
    }
}

/*
 *  Function to get logged in user id
 *  Return Boolean
 */
if (!function_exists('getContractorJob')) {

    function getContractorJob()
    {
        return \App\ContractorJob::where('contractor_id', getLogInUserId())->get()->pluck('job_id');
    }
}

/*
 *  Function to get Applied jobs in user id
 *  Return Boolean
 */
if (!function_exists('getBidJobs')) {

    function getBidJobs()
    {
        return \App\CompanyJobBidding::where('contractor_id', getLogInUserId())->get()->pluck('company_job_id');
    }
}

/*
 *  Function to get Applied jobs in user id
 *  Return Boolean
 */
if (!function_exists('getCompnayUsers')) {

    function getCompnayUsers()
    {
        if (isCompany()) {
            return \App\Models\User::where('id', getLogInUserId())->orWhere('parent_user_id', getLogInUserId())->get()->pluck('id');
        } else {
            return \App\Models\User::where('parent_user_id', getLogInUserId())->orWhere('parent_user_id', getMainParent())->get()->pluck('id');
        }
    }
}


/*
 *  Function to get random string
 *  @param lrngth
 *  Return string
 */
if (!function_exists('randomString')) {

    function randomString($length = 16,$onlystring='n')
    {
        $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if($onlystring == 'n'){
            $pool = '0123456789'.$pool;
        }
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }
}

/*
 *  Function to check user module access
 *  @param 1 module 
 *  @param 2 access (read,write,update,delete) 
 *  Return boolean
 */
if (!function_exists('checkUserModuleAccess')) {

    function checkUserModuleAccess($moduleId, $access)
    {
        if (getLogInUserRole() > 1) {
            $userAccess = \App\Models\UserModuleRight::where('module_id', $moduleId)->where('user_id', getLogInUserId())->first();
            if ($userAccess) {
                if ($userAccess->{$access} == 1) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return true; // Return true because if not rights given on user level than user can perform any task on module level
    }
}

/*
 *  Function to save company job log
 *  @param 1 company_job_id 
 *  @param 2 message
 *  Return log object
 */
if (!function_exists('addJobLog')) {

    function addJobLog($companyJobId, $message)
    {
        return \App\CompanyJobLog::saveJobLog($companyJobId, $message);
    }
}

/*
 *  Function to chnage base64to png image upload
 *  @param 1 base64string
 *  Return image path
 */
if (!function_exists('base64ToPng')) {

    function base64ToPng($base64_string, $output_file)
    {
        // open the output file for writing
        $ifp = fopen($output_file, 'wb');

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode(',', $base64_string);

        // we could add validation here with ensuring count( $data ) > 1
        fwrite($ifp, base64_decode($data[1]));

        // clean up the file resource
        fclose($ifp);

        return $output_file;
    }
}

if (!function_exists('generatePackagePurchaseLink')) {

    function generatePackagePurchaseLink($user_id, $package = 0)
    {
        $o_package_purchase_link = new \App\InvitationLink;
        $user = (new \App\Models\User)->find($user_id);
        $packagePurchaseLink = $o_package_purchase_link->generateInvitationLink(2, 'ppr_' . $user_id . '_' . date('Y-m-d H:i:s') . '_' . $package);
        $response = "";

        if (!empty($packagePurchaseLink)) {
            $package_purchase = $o_package_purchase_link->saveActivationLink($packagePurchaseLink, 2, $user_id);
            if ($package_purchase) {
                $package_purchase->email = $user->email;
                $package_purchase->save();
                $user->package_purchase_link = $package_purchase->link;
                $response = $package_purchase->link;
            } else {
                $response = "";
            }
        }

        return $response;
    }
}

if (!function_exists('getRequestAgentDetails')) {

    function getRequestAgentDetails()
    {
        $request = request();
        $user_agent = $request->header('User-Agent');
        $bname = 'Unknown';
        $platform = 'Unknown';

        //First get the platform?
        if (preg_match('/linux/i', $user_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $user_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $user_agent)) {
            $platform = 'windows';
        }

        // Next get the name of the useragent yes seperately and for good reason
        if (preg_match('/MSIE/i', $user_agent) && !preg_match('/Opera/i', $user_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Firefox/i', $user_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/Chrome/i', $user_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $user_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Opera/i', $user_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Netscape/i', $user_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        return ['browser' => $bname, 'user_agent' => $user_agent, 'platform' => $platform, 'ip_address' => $request->server('REMOTE_ADDR')];
    }
}

/*
 *  Function to get string of coupon duration
 *  Return String
 */
if (!function_exists('couponDurationString')) {

    function couponDurationString($coupon_duration)
    {
        $duration_string = $coupon_duration;
        $coupon_duration_parts = explode(' ', $coupon_duration);
        if (count($coupon_duration_parts) > 1) {
            $duration_string = $coupon_duration_parts[0] . ' ' . (intval($coupon_duration_parts[0]) > 1 ? $coupon_duration_parts[1] : rtrim($coupon_duration_parts[1], 's'));
        }
        return $duration_string;
    }
}

/*
 *  Function to get string of companies
 *  Return Companies Object
 */
if (!function_exists('getCustomers')) {

    function getCustomers()
    {
        return \App\Models\User::where('role_id', 3)->select(['id', 'name'])->get()->pluck('name', 'id');
    }
}

/*
 *  Function to check job is valid for logged in PM
 *  Return Companies Object
 */
if (!function_exists('isValidJobForPM')) {

    function isValidJobForPM($company_job)
    {
        if (isCompanyPM()) {
            $login_user = getLogInUser();
            $login_user->load('userProperties');
            $login_user_properties = ($login_user->userProperties->count() > 0 ? $login_user->userProperties->pluck('property_id')->toArray() : []);
            if (getMainParent() != $company_job->user_id) { // Not a valid company to view this job
                return false;
            } elseif (!in_array($company_job->property_id, $login_user_properties)) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}

/*
 *  Function to get company details from invitation code
 *  Return Companies Object
 */
if (!function_exists('companyFromInvitationCode')) {

    function companyFromInvitationCode($code)
    {
        $parent_user_id = 0;
        $companyData = explode('_', base64_decode($code));
        if (isset($companyData[1])) {
            $parent_user_id = $companyData[1];
        }
        return \App\Models\User::where('id', $parent_user_id)->with('companySettings')->first();
    }
}

/*
 *  Function to clean string to remove special characters
 *  Return Companies Object
 */
if (!function_exists('cleanString')) {

    /* Clean String */
    function cleanString($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
}

/* job statuses */
if (!function_exists('jobApplicationStatus')) {
    function jobApplicationStatus($status = 0)
    {
        $job_application_status = \Config::get('custom.job-application-status');
        return $job_application_status[$status];
    }
}

/* change date format */
if (!function_exists('changeDateFormat')) {
    function changeDateFormat($date, $format = "")
    {
        if ($format != "") {
            return date($format, strtotime($date));
        } else {
            return date("F d, Y", strtotime($date));
        }
    }
}

/* CMS pages type */
if (!function_exists('getCmsPagesType')) {
    function getCmsPagesType($type = null, $exceptionType = null)
    { //18 others , $exceptionType must be array
        $cms_page_types = \config::get('custom.cms_page_types');
        if (!$type && !$exceptionType) {
            return $cms_page_types;
        } else if ($type && !$exceptionType) {
            return $cms_page_types[$type];
        } else if (!$type && $exceptionType) {
            if (is_array($exceptionType)) {
                foreach ($exceptionType as $etype) {
                    if ($etype != 0) {
                        unset($cms_page_types[$etype]);
                    }
                }
            }
            return $cms_page_types;
        }

        return '-';
    }
}

if (!function_exists('getCmsPages')) {
    function getCmsPages()
    {
        return \App\CmsPages::where('status', 1)->pluck('slug', 'cms_page_type');
    }
}


if (!function_exists('getDropifyMaxFileSize')) {
    function getDropifyMaxFileSize($require = 'size')
    {
        if ($require == "label") {
            return '2MB';
        } else {
            return '2M';
        }
    }
}


if (!function_exists('getSiteSetting')) {
    function getSiteSetting($key){
        return \App\SiteSetting::where('setting_key',$key)->where('status',1)->first();
    }
}

if (!function_exists('validateCouponAndApply')) {
    function validateCouponAndApply($total,$code,$form_id){
        $total = floatval($total);
        $currentDate=\Carbon\Carbon::now()->toDateString();
        //get detail from coupon code
        $coupon=\App\Coupon::where('code',trim($code))->where('from_date','<=',$currentDate)->where('to_date','>=',$currentDate)->where('form_id',$form_id)->first();
        if($coupon){
            $coupon_amount = floatval($coupon->amount);
            $coupon_discount = 0;
            if($coupon->type==1){ //flat discount
                $coupon_value = $coupon_amount;
                $total= $total - $coupon_value;
                $coupon_discount = $coupon_amount;
                $coupon_value = '(-)$'.$coupon_value.' flat off';
            }else if($coupon->type==2){ //percentage discount
                $coupon_value = ($total*$coupon_amount)/100;
                $coupon_discount = $coupon_value;
                $total = $total - $coupon_value;
                $coupon_value = '(-)'.$coupon_value.' % off';
            }else{
                $coupon_value = 0;
            }
            $response=array(
                'status'=>1,
                'message'=>'Coupon successfully applied!',
                'coupon_value'=>$coupon_value,
                'coupon_discount'=>$coupon_discount,
                'coupon_title'=>$coupon->name,
                'net_total'=> round(floatval($total),2),
                'coupon' => [
                    'coupon_id' => $coupon->id,
                    'form_id' => $coupon->form_id,
                    'form_id' => $coupon->form_id,
                    'name' => $coupon->name,
                    'name' => $coupon->name,
                    'code' => $coupon->code,
                    'type' => $coupon->type,
                    'from_date' => $coupon->from_date,
                    'to_date' => $coupon->to_date,
                ]
            );
        }else{
            $response=array(
                'status'=>0,
                'message'=>'Invalid Coupon'
            );
        }
        return $response;
    }
}

if (!function_exists('calculateApplicationTax')) {
    function calculateApplicationTax($price){
        $tax_details = getSiteSetting('APP_TAX');
        $tax= ($tax_details ? round(((floatval($tax_details->setting_value)*floatval($price))/100),2) : 0);
        if(!empty($tax)){
            $total = number_format($price+$tax,2);    
        }        
        return ['tax_details'=>$tax_details,'tax'=>$tax,'total'=>$total];
    }
}

if(!function_exists('convertToemailTemplate')){
    function convertToemailTemplate($type,$object){
        $template_string = '';
        $email_template = \App\EmailTemplate::where('type',$type)->first();
        if($email_template){
            $template_string = $email_template->template;
            switch ($type) {
                case 1:
                    $template_words = ['[SMS_TEXT]'];
                    $template_word_replacements = [$object->sms_text];
                    $template_string = str_replace($template_words, $template_word_replacements, $template_string);
                    break;
                    
                default:
                    # code...
                    break;
            }
        }
        return $template_string;
    }
}
    /* change base64 to image */
if (!function_exists('getImageFromBase64ToImg')) {
    function getImageFromBase64ToImg($file)
    {
        $image_parts = explode(";base64,", $file);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
    
        $image_base64 = base64_decode($image_parts[1]);

        return  $image_base64;
    }
}

if(!function_exists('generateOTP')){
    function generateOTP(){
        $otp = mt_rand(10000,99999);
        return $otp;
    }
}

if(!function_exists('generateToken')){
    function generateToken($length = 10){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

/* Notify (Push Notification Object) */
if (!function_exists('notify')) {    
    function notify(){
        return new \App\Libraries\Notify;
    }
}

if(!function_exists('getNotificationType')){
    function getNotificationType($type){
      return  \Config::get('custom.notification_type.'.$type);
    }
}

if(!function_exists('edText')){
    function edText($text,$action){
        $response = '';
        $secret_key = config('custom.aes-secret-key');
        $action = strtolower($action);
        if($secret_key && in_array($action, ['decrypt','encrypt'])){
            $edData = \App\Libraries\AesCipher::$action($secret_key, $text);
            if($edData){
                $response = $edData->getData();
            }
        }
        return $response;
    }
}

if(!function_exists('generateUniqueSlug')){
    function generateUniqueSlug($model,$slug,$id='')
    {
        if($id!=''){
            $check_slug = $model::where('slug', $slug)->where('id','!=',$id)->first();
        }else{
            $check_slug = $model::where('slug', $slug)->first();
        }
        if (!$check_slug) {
            return strtolower($slug);
        }
        return $this->generateUniqueSlug($slug.'-'.time(),$id);
    }
}

/* Create Dynamic Database for City State Country */
function saveDynamicLocations($location_data){
    $response = [
        'country_id' => NULL,
        'state_id' => NULL,
        'city_id' => NULL,
    ];

    $location_data = ($location_data ? (array)$location_data : []);
    // Save Country
    if(array_key_exists('country', $location_data) || array_key_exists('country_code', $location_data)){

        if(isset($location_data['country_code']) && strtolower($location_data['country_code']) == 'in'){
            $location_data['country'] = 'India';   
        }
        $country = (new \App\Models\Country)->isCountryExits($location_data['country']);
        if(!$country){
            $country = new \App\Models\Country;
            $country->name = ucwords(strtolower($location_data['country']));
            $country->code = (isset($location_data['country_code']) ? $location_data['country_code'] : NULL);
            $country->save();
        }
        $response['country_id'] = $country->id;

        // Save State
        if(array_key_exists('state', $location_data)){

            $state = (new \App\Models\State)->isStateExits($location_data['state'],$country->id);
            if(!$state){
                $state = new \App\Models\State;
                $state->name = ucwords(strtolower($location_data['state']));
                $state->country_id = $country->id;
                $state->code = (isset($location_data['state_code']) ? $location_data['state_code'] : NULL);
                $state->save();
            }
            $response['state_id'] = $state->id;

            // Save City
            if(array_key_exists('city', $location_data)){

                $city = (new \App\Models\City)->isCityExits($location_data['city'],$state->id);
                if(!$city){
                    $city = new \App\Models\City;
                    $city->name = ucwords(strtolower($location_data['city']));
                    $city->state_id = $state->id;
                    $city->save();
                }
                $response['city_id'] = $city->id;
            }

        }

    }

    return $response;
}