<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

     /* Set Flash Message To Flash after redirect */
     public function setFlash($message,$status,$subtitle=''){
        \Session::put('flash-message',$message);
        \Session::put('flash-status',$status);
        \Session::put('flash-subtitle',$subtitle);
    }
    /* End */

    /* Function to check access rights for this requested module */
    protected function authoriseBackendRequest($module,$accessType,$returnRedirect=false){
        $backendUser = \Session::get('backend-user');
        if($backendUser->role_id == 1){
            return true;
        }else{
            $accessibleModules = \Session::get('accessibleModules');
            if($accessibleModules->count()){
                $accessAllowed = $accessibleModules->filter(function($item) use($module) {
                    return $item->module_id == $module;
                })->first();
                if(!$accessAllowed){
                    if($returnRedirect){
                        abort(401);
                    }else{
                        return false;
                    }
                }else{
                    // User Level Access
                    $accessResponse = checkUserModuleAccess($module,$accessType);
                    if(!$accessResponse){
                        if($returnRedirect){
                           abort(401);
                        }else{
                            return false;
                        }
                    }else{
                        return true;
                    }
                }
            }
        }
    }
}
