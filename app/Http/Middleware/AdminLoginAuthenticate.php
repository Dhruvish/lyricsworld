<?php

namespace App\Http\Middleware;

use Closure;

class AdminLoginAuthenticate
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Request::route()->getName() == '/') {
            return redirect(route('home'));
        }
        $loginRoutes = \Config::get('custom.login-routes-name');
        if (!empty(\Session::get('backend-access')) && !empty(\Session::get('backend-user-id'))) {
            $userId = \Session::get('backend-user-id');
            //$userData = \App\Models\User::where([['id', '=', $userId], ['status_id', '=', 1]])->with('userCompany')->first();
            $userData = \App\Models\User::where([['id', '=', $userId], ['status', '=', 'active']])->with('userCompany')->first();
            \Session::put('backend-user', $userData);
            if (!$userData) {
                if (\Request::route()->getName() == 'backend.logout') {
                    return $next($request);
                } else {
                    return redirect('backend-logout');
                }
            }
            if (in_array(\Request::route()->getName(), $loginRoutes)) {
                return redirect(route('dashboard'));
            } else {
                $master_modules = \Session::get('master_modules');
                if (empty($master_modules)) {
                    // Module Rights
                    $accessibleModules = \App\Models\ModuleRight::where([['role_id', '=', $userData->role_id]])->with('module')->get();

                    // Set session to check modules which are accessible 
                    \Session::put('accessibleModules', $accessibleModules);

                    $moduleRights = $accessibleModules->pluck('module_id');

                    // Module Access Authentication                
                    $master_modules = \App\Models\Module::where([['parent_module', '=', 0], ['status_id', '=', 1]])->orderBy('sort_order');

                    if ($userData->role_id > 1) {
                        $master_modules->whereIn('id', $moduleRights);
                    }

                    $master_modules->with(['subModules' => function ($query) use ($moduleRights, $userData) {
                        $query->where('status_id', 1);
                        if ($userData->role_id > 1) {
                            $query->whereIn('id', $moduleRights);
                        }
                    }]);

                    $master_modules = $master_modules->get();
                    \Session::put('master_modules', $master_modules);
                }

                if ($userData->role_id > 1) {
                    /* Check for access to current route */
                    $allowed_rights = \Config::get('custom.default-routes');
                    // dd($moduleRights);
                }
                
                \View::share('userData', $userData);
                \View::share('master_modules', $master_modules);
                return $next($request);
            }
        } else {
            if (in_array(\Request::route()->getName(), $loginRoutes)) {
                return $next($request);
            } else {
                return redirect(route('admin-panel.login'));
            }
        }
        abort(404);
    }
}
