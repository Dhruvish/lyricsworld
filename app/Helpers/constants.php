<?php
if (!defined('APP_ROOT')) {
    define('APP_ROOT',env('APP_URL').'/');
}
if (!defined('BE_URL')) {
    define('BE_URL', env('APP_URL').'/backend'.'/');
}
if (!defined('APP_NAME')) {
    define('APP_NAME', env('APP_NAME', 'LyricWorld'));
}

// define('SUPER_ADMIN',1);
// define('SUB_ADMIN',2);
// define('ACTIVE', 'active');
// define('INACTIVE', 'inactive');
?>