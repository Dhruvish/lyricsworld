<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('name');
            $table->integer('country_id')->unsigned();
            $table->Foreign('country_id')->references('id')->on('countries');
            $table->string('code')->nullable()->default(NULL);
            $table->timestamps();
            $table->SoftDeletes();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
