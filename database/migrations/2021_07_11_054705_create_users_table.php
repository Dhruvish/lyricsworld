<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('slug')->nullable()->default(NULL);
            $table->string('email');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable()->default(NULL);
            $table->string('reset_password')->nullable()->default(NULL);
            $table->string('phone')->nullable()->default(NULL);
            $table->text('profile_pic')->nullable()->default(NULL);
            $table->integer('status')->unsigned()->default(0)->comment="0 - Pending, 1 - Active, 2 - Reject";
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->datetime('last_login_at')->nullable()->default(NULL);
            $table->string('last_login_ip',50)->nullable()->default(NULL);
            $table->integer('created_by')->nullable()->default(0);
            $table->integer('last_updated_by')->nullable()->default(0);
            $table->rememberToken();
            $table->timestamps();
            $table->SoftDeletes();
            $table->string('ip_address')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
