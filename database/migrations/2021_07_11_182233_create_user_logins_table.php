<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_logins', function (Blueprint $table) {
            $table->Increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('ip_address');
            $table->string('user_agent');
            $table->string('platform');
            $table->string('login_token')->nullable()->default(NULL);
            $table->integer('login_token_status')->default(0)->comment="0 - Inactive, 1 - Active";
            $table->string('mac_address')->nullable()->default(NULL);
            $table->text('device_token')->nullable();
            $table->tinyInteger('device_type')->default(0)->comment="1 -Android, 2 -iOS";
            $table->timestamps();
            $table->SoftDeletes();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_logins');
    }
}
