<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('display_name');
            $table->string('name');
            $table->integer('parent_module')->default(0);
            $table->tinyInteger('type')->default(0)->comment="0 - Available for module access, 1 - Only for admin";
            $table->string('link',50)->nullable()->default(null);
            $table->tinyInteger('status_id')->default(0)->comment="1 - Active, 0 - Inactive";
            $table->string('icon');
            $table->string('classes');
            $table->tinyInteger('sort_order')->unsigned()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
