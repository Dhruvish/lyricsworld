<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('name');
            $table->integer('state_id')->unsigned();
            $table->Foreign('state_id')->references('id')->on('states');
            $table->string('code')->nullable()->default(NULL);
            $table->string('latitude',80)->nullable()->default(NULL);
            $table->string('longitude',80)->nullable()->default(NULL);
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
