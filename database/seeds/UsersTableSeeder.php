<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        \App\Models\User::insert(
        	[
                ['first_name'=>'Super Admin','last_name'=>'Admin','email'=>'admin@lyricsworld.in','password'=>\Hash::make('123456'),'status'=>'active','role_id'=>1], 
            ]
        );
    }
}
