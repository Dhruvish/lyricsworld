<script src="/admin-assets/js/jquery-3.4.1.min.js"></script>

		<!-- BOOTSTRAP JS -->
		<script src="/admin-assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="/admin-assets/plugins/bootstrap/js/popper.min.js"></script>

		<!-- SPARKLINE JS-->
		<script src="/admin-assets/js/jquery.sparkline.min.js"></script>

		<!-- CHART-CIRCLE JS-->
		<script src="/admin-assets/js/circle-progress.min.js"></script>

		<!-- RATING STARJS -->
		<script src="/admin-assets/plugins/rating/jquery.rating-stars.js"></script>

		<!-- EVA-ICONS JS -->
		<script src="/admin-assets/iconfonts/eva.min.js"></script>

		<!-- INTERNAL CHARTJS CHART JS -->
		<script src="/admin-assets/plugins/chart/Chart.bundle.js"></script>
		<script src="/admin-assets/plugins/chart/utils.js"></script>

		<!-- INTERNAL PIETY CHART JS -->
		<script src="/admin-assets/plugins/peitychart/jquery.peity.min.js"></script>
		<script src="/admin-assets/plugins/peitychart/peitychart.init.js"></script>

		<!-- SIDE-MENU JS-->
		<script src="/admin-assets/plugins/sidemenu/sidemenu.js"></script>

		<!-- PERFECT SCROLL BAR js-->
		<script src="/admin-assets/plugins/p-scroll/perfect-scrollbar.min.js"></script>
		<script src="/admin-assets/plugins/sidemenu/sidemenu-scroll.js"></script>

		<!-- CUSTOM SCROLLBAR JS-->
		<script src="/admin-assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

		<!-- SIDEBAR JS -->
		<script src="/admin-assets/plugins/sidebar/sidebar.js"></script>

		<!-- INTERNAL APEXCHART JS -->
		<script src="/admin-assets/js/apexcharts.js"></script>

		<!--INTERNAL  INDEX JS -->
		<script src="/admin-assets/js/index1.js"></script>

		<!-- CUSTOM JS -->
		<script src="/admin-assets/js/custom.js"></script>