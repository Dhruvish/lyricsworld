@extends('admin.templete.layouts.master')

@section('title','dashboard')

@section('inner_page_link')
<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
@stop

@section('content')
<!-- Row -->
<div class="row">
   <div class="col-xl-3 col-sm-6">
      <div class="card">
         <div class="card-body">
            <div class="row mb-1">
               <div class="col">
                  <p class="mb-1">Product Sold</p>
                  <h3 class="mb-0 number-font">57,865</h3>
               </div>
               <div class="col-auto mb-0">
                  <div class="dash-icon text-orange">
                     <i class="bx bxs-wallet"></i>
                  </div>
               </div>
            </div>
            <span class="fs-12 text-muted"> <strong>2.6%</strong><i class="mdi mdi-arrow-up"></i> <span class="text-muted fs-12 ml-0 mt-1">than last week</span></span>
         </div>
      </div>
   </div>
   <div class="col-xl-3 col-sm-6">
      <div class="card">
         <div class="card-body">
            <div class="row mb-1">
               <div class="col">
                  <p class="mb-1">Total Balance</p>
                  <h3 class="mb-0 number-font">$2,156</h3>
               </div>
               <div class="col-auto mb-0">
                  <div class="dash-icon text-secondary1">
                     <i class="bx bxs-wallet"></i>
                  </div>
               </div>
            </div>
            <span class="fs-12 text-muted"> <strong>0.06%</strong><i class="mdi mdi-arrow-down"></i> <span class="text-muted fs-12 ml-0 mt-1">than last week</span></span>
         </div>
      </div>
   </div>
   <div class="col-xl-3 col-sm-6">
      <div class="card">
         <div class="card-body">
            <div class="row mb-1">
               <div class="col">
                  <p class="mb-1">Sales Profit</p>
                  <h3 class="mb-0 number-font">$12,105</h3>
               </div>
               <div class="col-auto mb-0">
                  <div class="dash-icon text-secondary">
                     <i class="bx bxs-badge-dollar"></i>
                  </div>
               </div>
            </div>
            <span class="fs-12 text-muted"> <strong>0.15%</strong><i class="mdi mdi-arrow-down"></i> <span class="text-muted fs-12 ml-0 mt-1">than last week</span></span>
         </div>
      </div>
   </div>
   <div class="col-xl-3 col-sm-6">
      <div class="card">
         <div class="card-body">
            <div class="row mb-1">
               <div class="col">
                  <p class="mb-1">Total Expenses</p>
                  <h3 class="mb-0 number-font">$4,673</h3>
               </div>
               <div class="col-auto mb-0">
                  <div class="dash-icon text-warning">
                     <i class="bx bxs-credit-card-front"></i>
                  </div>
               </div>
            </div>
            <span class="fs-12 text-muted"> <strong>1.05%</strong><i class="mdi mdi-arrow-up"></i> <span class="text-muted fs-12 ml-0 mt-1">than last week</span></span>
         </div>
      </div>
   </div>
</div>
<!-- Row-1 End -->	
@stop