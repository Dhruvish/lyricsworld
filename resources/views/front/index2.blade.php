@extends('front.templete.layout')

@section('title','index')
@section('content')
<div class="lyric-search">
			<div class="container pos-center">
				<h1>Search 386 Song Lyrics </h1>
					<div class="lyric-search-input">
						<form role="search" action="http://www.2035themes.com/muusico/" method="get">
							<div class="search-wrapper clearfix">
								<div class="pull-left lsearchl">
									<input class="lisinput" type="search" name="s" placeholder="Search: Enter lyrics name keywords hit enter" />
								</div>
								<div class="pull-left lsearchr">
									<label for="SongSearch" class="slabel"><i class="fa fa-search"></i></label>
									<input id="SongSearch" type="submit" value="" style="display:none;" />
								</div>
								<div class="searchcats">
									<ul>
										<li><input type="radio" name="post_type" value="lyrics" checked>Lyrics</li>
										<li><input type="radio" name="post_type" value="album">Albums</li>
										<li><input type="radio" name="post_type" value="artist">Artists</li>
									</ul>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>		
		</div>
		<div class="container content-capsule"> 
			<div class="content-pull">
				<div class="height-capsule">
					<div class="popular-lyrics-container clearfix">
						<div class="container">
							<div class="popular-lyrics clearfix">
								<div class="title">
									<h3><i class="fa fa-star"></i>POPULAR LYRICS</h3>
								</div>
								<div class="full-screen-list clearfix margint10">
									<ul id="popular-listr">
										<li class="gradient"  style="background:url('/front/wp-content/uploads/2015/02/image-14-big-min-412x548.jpg') no-repeat;">
											<a href="/front/lyrics/smooth-criminal-3/index.html">
												<div class="item"></div>
												<div class="item-text">
													<h3>
														<a href="/front/lyrics/smooth-criminal-3/index.html">Smooth Criminal</a>
													</h3>
													<a href="/front/artist/michael-jackson-3/index.html">Michael Jackson</a>
												</div>
											</a>
										</li>
										<li class="gradient"  style="background:url('/front/wp-content/uploads/2015/02/image-14-small-min.jpg') no-repeat;">
											<a href="/front/lyrics/bailando/index.html">
												<div class="item"></div>
												<div class="item-text">
													<h3><a href="/front/lyrics/bailando/index.html">Bailando</a></h3>
													<a href="/front/artist/sia/index.html">Sia</a>
												</div>
											</a>
										</li>
										<li class="gradient"  style="background:url('/front/wp-content/uploads/2015/02/image-20-small-min.jpg') no-repeat;">
											<a href="/front/lyrics/all-day/index.html">
												<div class="item"></div>
												<div class="item-text">
													<h3><a href="/front/lyrics/all-day/index.html">All Day</a></h3>
													<a href="/front/artist/jessie-j/index.html">Jessie J</a> ft. <a href="/front/artist/rihanna/index.html">Rihanna</a>
												</div>	
											</a>
										</li>
										<li class="gradient"  style="background:url('/front/wp-content/uploads/2015/02/image-10-small-min.jpg') no-repeat;">
											<a href="/front/lyrics/fix-you/index.html">
												<div class="item"></div>
												<div class="item-text">
													<h3><a href="/front/lyrics/fix-you/index.html">Fix You</a></h3>
													<a href="artist/christina-aguilera/index.html">Christina Aguilera</a>
												</div>
											</a>
										</li>
										<li class="gradient"  style="background:url('/front/wp-content/uploads/2015/02/image-11-small-min.jpg') no-repeat;">
											<a href="lyrics/sugar/index.html">
												<div class="item"></div>
												<div class="item-text">
													<h3><a href="lyrics/sugar/index.html">Sugar</a></h3>
													<a href="artist/beyonce/index.html">Beyonce</a>
												</div>
											</a>
										</li>
										<li class="gradient"  style="background:url('/front/wp-content/uploads/2015/02/image-18-small-min.jpg') no-repeat;">
											<a href="lyrics/suerte-whenever-wherever/index.html">
												<div class="item"></div>
												<div class="item-text">
													<h3><a href="lyrics/suerte-whenever-wherever/index.html">Suerte (Whenever, Wherever)</a></h3>
													<a href="artist/shakira/index.html">Shakira</a>
												</div>
											</a>
										</li>
										<li class="gradient"  style="background:url('/front/wp-content/uploads/2015/02/image-1-small-min.jpg') no-repeat;">
											<a href="lyrics/someone-like-you-2/index.html">
												<div class="item"></div>
												<div class="item-text">
													<h3><a href="lyrics/someone-like-you-2/index.html">Someone Like You</a></h3>
													<a href="artist/adele/index.html">Adele</a>
												</div>
											</a>
										</li>
										<li class="gradient"  style="background:url('/front/wp-content/uploads/2015/02/image-13-small-min.jpg') no-repeat;">
											<a href="lyrics/super-bass-2/index.html">
												<div class="item"></div>
												<div class="item-text">
													<h3><a href="lyrics/super-bass-2/index.html">Super Bass</a></h3>
													<a href="artist/nicki-minaj/index.html">Nicki Minaj</a>
												</div>
											</a>
										</li>
										<li class="gradient"  style="background:url('/front/wp-content/uploads/2015/02/image-12-small-min.jpg') no-repeat;">
											<a href="lyrics/break-free/index.html">
												<div class="item"></div>
												<div class="item-text">
													<h3><a href="lyrics/break-free/index.html">Break Free</a></h3>
													<a href="artist/enrique-iglesias/index.html">Enrique Iglesias</a>
												</div>
											</a>
										</li>
										<li class="gradient"  style="background:url('/front/wp-content/uploads/2015/02/image-9-small-min.jpg') no-repeat;">
											<a href="lyrics/a-sky-full-of-stars/index.html">
												<div class="item"></div>
												<div class="item-text">
													<h3><a href="lyrics/a-sky-full-of-stars/index.html">A Sky Full of Stars</a></h3>
													<a href="artist/demi-lovato/index.html">Demi Lovato</a>
												</div>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="container pos-center">
						<div class="homepage-advertisement">
							<img src="front/ads/index-ads.jpg"/>          
						</div>
					</div>
					<div class="container">
						<div class="homepage-news clearfix">
							<div class="latestnew title">
								<h3><i class="fa fa-clock-o"></i>LATEST NEWS</h3>
							</div>
							<div class="col-lg-4 col-sm-4 col-xs-12">
								<img width="820" height="400" src="/front/wp-content/uploads/2015/01/post-1-820x400.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" />              
								<a href="example-category/enrique-iglesias-biography/index.html"><h4>Enrique Iglesias Biography</h4></a>
								<p>Born in Spain in 1975, Enrique Iglesias is the son of popular Spanish singer Julio Iglesias. Iglesias grew up largely in Miami and began singing as a teenager. He released his self-titled debut album in 1995 and, like his subsequent studio works, proved to be a huge success. By early 2012, Iglesias had sold more [&hellip;]</p>
								<a href="example-category/enrique-iglesias-biography/index.html"><strong>Read More</strong></a>
							</div>	
							<div class="col-lg-4 col-sm-4 col-xs-12">
								<img width="820" height="400" src="/front/wp-content/uploads/2015/01/post-3-820x400.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" />              <a href="uncategorized/shakira-biography/index.html"><h4>Shakira Biography</h4></a>
								<p>Synopsis Born in Colombia on February 2, 1977, hugely successfully Colombian pop singer and dancer Shakira has won two Grammy Awards, seven Latin Grammy Awards and 12 Billboard Latin Music Awards, and has been nominated for a Golden Globe Award. Known for hits like &#8220;Whenever, Wherever&#8221; and &#8220;Hips Don&#8217;t Lie,&#8221; Shakira is the highest-selling Colombian [&hellip;]</p>
								<a href="uncategorized/shakira-biography/index.html"><strong>Read More</strong></a>
							</div>
							<div class="col-lg-4 col-sm-4 col-xs-12">
								<img width="820" height="400" src="/front/wp-content/uploads/2015/01/post-2-820x400.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" />              <a href="uncategorized/rihanna-biography/index.html"><h4>Rihanna Biography</h4></a>
								<p>Synopsis Born Robyn Rihanna Fenty, on February 20, 1988 in Barbados, Rihanna signed with Def Jam records at age 16 and released her first album, which sold more than 2 million copies worldwide, in 2005. She went on to release more albums and hit songs, including &#8220;Unfaithful,&#8221; &#8220;Disturbia&#8221; and &#8220;Umbrella.&#8221; Rihanna has also won multiple [&hellip;]</p>
								<a href="uncategorized/rihanna-biography/index.html"><strong>Read More</strong></a>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="latest-lyrics-container clearfix">
							<div class="title">
								<h3><i class="fa fa-clock-o"></i>LATEST LYRICS</h3>
							</div>
							<div class="latest-lyrics margint10">
								<ul>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/smooth-criminal-3/index.html">Smooth Criminal</a>
											</h5>
											<a href="artist/michael-jackson-3/index.html">Michael Jackson</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/all-day/index.html">All Day</a>
											</h5>
											<a href="artist/jessie-j/index.html">Jessie J</a> ft. <a href="artist/rihanna/index.html">Rihanna</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/lucky/index.html">Lucky</a>
											</h5>
											<a href="artist/fergie/index.html">Fergie</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/fix-you/index.html">Fix You</a>
											</h5>
											<a href="artist/christina-aguilera/index.html">Christina Aguilera</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/a-sky-full-of-stars/index.html">A Sky Full of Stars</a>
											</h5>
											<a href="artist/demi-lovato/index.html">Demi Lovato</a>
									</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/break-free/index.html">Break Free</a>
											</h5>
											<a href="artist/enrique-iglesias/index.html">Enrique Iglesias</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/super-bass-2/index.html">Super Bass</a>
											</h5>
											<a href="artist/nicki-minaj/index.html">Nicki Minaj</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/part-of-your-world/index.html">Part Of Your World</a>
											</h5>
											<a href="artist/iggy-azalea/index.html">Iggy Azalea</a>
									</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/anaconda-3/index.html">Anaconda</a>
											</h5>
											<a href="artist/nicki-minaj/index.html">Nicki Minaj</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/one-last-time/index.html">One Last Time</a>
											</h5>
											<a href="artist/hozier/index.html">Hozier</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/a-team/index.html">A Team</a>
											</h5>
											<a href="artist/ed-sheeran/index.html">Ed Sheeran</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/demons/index.html">Demons</a>
											</h5>
											<a href="artist/lana-del-rey/index.html">Lana Del Rey</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/price-tag-2/index.html">Price Tag</a>
											</h5>
											<a href="artist/jessie-j/index.html">Jessie J</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/shake-it-off-2/index.html">Shake It Off</a>
											</h5>
											<a href="artist/taylor-swift/index.html">Taylor Swift</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/boom-clap/index.html">Boom Clap</a>
											</h5>
											<a href="artist/jessie-j/index.html">Jessie J</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/someone-like-you-2/index.html">Someone Like You</a>
											</h5>
											<a href="artist/adele/index.html">Adele</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/style-2/index.html">Style</a>
											</h5>
											<a href="artist/taylor-swift/index.html">Taylor Swift</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/chandelier-3/index.html">Chandelier</a>
											</h5>
											<a href="artist/sia/index.html">Sia</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/chandelier-2/index.html">The Hanging Tree</a>
											</h5>
											<a href="artist/sia/index.html">Sia</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/sugar-2/index.html">Sugar</a>
											</h5>
											<a href="artist/maroon-5/index.html">Maroon 5</a>
										</div>
									</li>
									<li>
										<div class="lyric-content">
											<h5>
											<a href="front/lyrics/thinking-out-loud-2/index.html">Thinking Out Loud</a>
											</h5>
											<a href="artist/ed-sheeran/index.html">Ed Sheeran</a>
									</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="footer clearfix"><!-- Footer -->
					@include('front.templete.footer')
				</div>
			</div><!-- Content Pull Top -->
		</div>

@endsection