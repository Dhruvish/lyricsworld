
	<div class="pull-left footer-logo">
		<img alt="Logo" src="front//wp-content/themes/muusico/images/logo-footer.png" ></a>
		<div class="clearfix"></div>
	</div>
	<div class="pull-right footer-menu">
		<ul id="nav" class="sf-menu">
			<li id="menu-item-1844" class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1784 current_page_item"><a href="{{ route('index') }}">Home</a></li>
			<li id="menu-item-1817" class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="about-2/index.html">About</a></li>
			<li id="menu-item-1841" class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="artists/index.html">Artists</a></li>
			<li id="menu-item-1830" class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="albums/index.html">Albums</a></li>
			<li id="menu-item-2787" class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="genres/index.html">Genres</a></li>
			<li id="menu-item-1849" class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="blog/index.html">News</a></li>
			<li id="menu-item-1824" class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="contact/index.html">Contact</a></li>
		</ul> 
	</div>
</div>
