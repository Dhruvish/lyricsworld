<div class="blog-back">
	<div class="container">
		<div class="row">
			<div class="nav-top menu-container">  
				<div class="main-header clearfix">
					<div class="logo pull-left">
						<div class="limg">
							<a href="{{route('index')}}">
								<img alt="logo" class="img-responsive" src="/front/wp-content/themes/muusico/images/logo.png" />
							</a>
						</div>
					</div>
					<div class="pull-right">
						<div class="pull-left">
							<div id="navigation-menu" class="menu">
								<nav id="menu">
									<ul id="nav" class="sf-menu navigate">
										<li id="menu-item-1844" class=" menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1784 current_page_item"><a href="{{route('index')}}">Home</a></li>
										<!-- <li id="menu-item-1817" class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="/front/about-2/index.html">About</a></li>
										<li id="menu-item-1841" class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="/front/artists/index.html">Artists</a></li>
										<li id="menu-item-1830" class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="/front/albums/index.html">Albums</a></li>
										<li id="menu-item-2787" class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="/front/genres/index.html">Genres</a></li>
										<li id="menu-item-1849" class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="front/blog/index.html">News</a></li>-->
										<!-- <li id="menu-item-1824" class=" menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ route('front-contact') }}">Contact</a></li> -->
									</ul>   
								</nav>
							</div>
						</div>
						<div class="pull-left header-search">
							<a id="search-button" href="#"><i class="fa fa-search open-search"></i></a>
							<div id="mini-search-wrapper">
								<form role="search" action="" method="get">
									<input type="search" id="s" name="s" class="s-input" required />
									<input type="submit" class="s-submit" value="Search" />
									<div class="searchminicats">
										<ul>
											<li><input type="radio" name="post_type" value="lyrics" checked>Lyrics</li>
											<li><input type="radio" name="post_type" value="album">Albums</li>
											<li><input type="radio" name="post_type" value="artist">Artists</li>
										</ul>
									</div>
								</form>
							</div>
						</div>
						<div class="pull-right social-icons">
							<ul>
								<li><a href="http://twitter.com/2035themes"><i  alt="Twitter Site URL" class="fa fa-twitter"></i></a></li>
								<li><a href="https://soundcloud.com/2035themes"><i class="fa fa-soundcloud"></i></a></li>
								<li><a href="http://spotify.com/2035themes"><i class="fa fa-spotify"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="social-media pull-right">
						<div id="mobile-header">
							<div id="responsive-menu-button">
								<span class="top"></span>
								<span class="middle"></span>
								<span class="middlecopy"></span>
								<span class="bottom"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>    