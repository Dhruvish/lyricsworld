<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta charset="UTF-8">
<meta name="author" content="">
@include('front.templete.headercss')
</head>
<body class="home page-template page-template-homepage page-template-homepage-php page page-id-1784">
	<div id="wrapper" class="fitvids ">
	<div class="lyric-home" style="background: url(/front/wp-content/uploads/2015/01/homepage-min.jpg) center center; background-size:cover;" >
		@include('front.templete.header')
		@yield('content')
	</div>
	@include('front.templete.footerjs')
</body>
</html>

