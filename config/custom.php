<?php

return [
	'info-mail' => 'info@lyricsworld.com',
	'admin-mail' => [(env('APP_ENV') == 'production' ? 'admin@lyricsworld.com' : 'test@gmail.com'), 'test@gmail.com'],
	'info-mail' => 'info@lyricsworld.com',
	'login-routes' => [
		'/', 'backend-login', '', 'backend-login-set',
	],
	'login-routes-name' => [
		'/', 'admin-panel.login', 'backend.login.set',
	],
	'front-login-routes' => [
		'signin', 'signin.processed', 'signup', 'signup.processed', 'forgot.password', 'regenerate.password',
	],		
	'default-routes' => [
		'dashboard',
	],
	[
		'access-rights' => [
			1 => [ // Super Admin Rights
				'openForAll' => true,
			],
			2 => [
				'openForAll' => false,
				'modules' => [

				],
			],
		],
	],
	'pagination' => [
		'per_page' => 10,
	]
];